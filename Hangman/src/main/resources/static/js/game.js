var letters = ['A', 'B', 'C', 'Ç', 'D', 'E',
    'F', 'G', 'Ğ', 'H', 'I', 'İ', 'J', 'K',
    'L', 'M', 'N', 'O', 'Ö', 'P', 'R',
    'S', 'Ş', 'T', 'U', 'Ü', 'V',
    'Y', 'Z', '?'
];

function addWord() {
    const pathNames = $(location).attr('pathname').split('/');
    const gameId = pathNames[pathNames.length - 1];
    $.get("/hangman/v1/api/game/" + gameId)
        .done(function (result) {
            if(result.genericObject.gameStatus.localeCompare("Kaybedildi") == 0){
                disableAllButtons();
                document.body.style.backgroundColor = "red";
            }
            else if(result.genericObject.gameStatus.localeCompare("Kazanıldı") == 0){
                disableAllButtons();
                document.body.style.backgroundColor = "green";
            }
            // just disable buttons that already tried
            for(let i=0 ; i<result.genericObject.incorrectLetters.length;i++){
                document.getElementById("btn"+result.genericObject.incorrectLetters[i]).disabled = true;
            }
            createLettersBar(result);
        }).fail(function (error) {
        alert(JSON.stringify(error));
    });
}

function createLettersBar(game) {
    const imgHangman = $('#imgHangman');
    imgHangman.attr("src", "/images/hangman/" + game.genericObject.guessesLeft + ".PNG");
    const wordContentTable = $('#wordContent');
    wordContentTable.empty();
    const wordTr = $('<tr style="margin: 5px;"></tr>');

    for (let i = 0; i < game.genericObject.guessedWord.length; i++) {
        const wordTd = $('<td style="margin: 5px;"></td>');
        const wordLabel = $('<label class="btn btn-primary"></label>');
        wordTd.append(wordLabel);
        wordTr.append(wordTd);
        wordLabel.text(game.genericObject.guessedWord[i]);
    }
    const wordIncorrectTr = $('<tr style="margin: 5px;"></tr>');

    for (let i = 0; i < game.genericObject.incorrectLetters.length; i++) {
        const wordTd = $('<td style="margin: 5px;"></td>');
        const wordLabel = $('<label class="btn btn-primary"></label>');
        wordTd.append(wordLabel);
        wordIncorrectTr.append(wordTd);
        wordLabel.text(game.genericObject.incorrectLetters[i]);
    }
    const emptyLine = $('<tr style="margin: 5px;"><td><br/></td></tr>');
    wordContentTable.append(wordTr);
    wordContentTable.append(emptyLine);
    wordContentTable.append(wordIncorrectTr);
}


function sendPutRequest(letter) {
    const pathNames = $(location).attr('pathname').split('/');
    const gameId = pathNames[pathNames.length - 1];
    var obj = {guessedWord: letter};
    $.ajax({
        type: 'PUT',
        url: 'http://localhost/hangman/v1/api/game/' + gameId,
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(obj),
    }).done(function (result) {
        document.getElementById("btn"+letter).disabled = true;
        createLettersBar(result);
        if(result.genericObject.gameStatus.localeCompare("Kaybedildi") == 0){
            disableAllButtons();
            document.body.style.backgroundColor = "red";
        }
        else if(result.genericObject.gameStatus.localeCompare("Kazanıldı") == 0){
            disableAllButtons();
            document.body.style.backgroundColor = "green";
        }
    }).fail(function (msg) {
        console.log('FAIL');
    });
}

function disableAllButtons(){
    for(let i = 0;i<letters.length;i++){
        document.getElementById("btn"+letters[i]).disabled = true;
    }
}

function addLetterButtons() {
    const gameContentDiv = $('#gameContent');
    const letterButtonsDiv = $('<table  id="letterButtons" class="row"></table>');
    const letterButtonsTrLeft = $('<tr style="margin: 5px;"></tr>');
    const letterButtonsTrRight = $('<tr style="margin: 5px;"></tr>');
    for (let i = 0; i < 15; i++) {
        const tableTd = $('<tr ></tr>');
        const letterButton = $('<button style="margin: 5px;" id="btn'+letters[i]+'" onclick="sendPutRequest(letters[' + i + ']);"  class="btn btn-primary btn-block">' + letters[i] + '</button>');
        tableTd.append(letterButton);
        letterButtonsTrLeft.append(tableTd);
    }
    for (let i = 15; i < letters.length; i++) {
        const tableTd = $('<tr ></tr>');
        const letterButton = $('<button style="margin: 5px;" id="btn'+letters[i]+'" onclick="sendPutRequest(letters[' + i + ']);"  class="btn btn-primary btn-block">' + letters[i] + '</button>');
        tableTd.append(letterButton);
        letterButtonsTrRight.append(tableTd);
    }
    letterButtonsDiv.append(letterButtonsTrLeft);
    letterButtonsDiv.append(letterButtonsTrRight);
    gameContentDiv.append(letterButtonsDiv);
}

addLetterButtons();
addWord();