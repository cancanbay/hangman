package com.cancanbay.hangman.exception;

public class HangmanException extends Exception {
    public HangmanException() {
    }

    public HangmanException(String message) {
        super(message);
    }

    public HangmanException(String message, Throwable cause) {
        super(message, cause);
    }

    public HangmanException(Throwable cause) {
        super(cause);
    }

    public HangmanException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
