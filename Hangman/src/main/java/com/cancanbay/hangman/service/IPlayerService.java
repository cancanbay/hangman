package com.cancanbay.hangman.service;

import com.cancanbay.hangman.model.Player;

public interface IPlayerService extends IGenericService<Player, Long> {
}
