package com.cancanbay.hangman.service;

import com.cancanbay.hangman.enumeration.GameStatus;
import com.cancanbay.hangman.exception.HangmanException;
import com.cancanbay.hangman.model.Game;
import com.cancanbay.hangman.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GameService extends AbstractService implements IGameService {
    @Autowired
    private GameRepository gameRepository;

    @Override
    public List<Game> getAll() throws HangmanException {
        try {
            return gameRepository.findAll();
        } catch (Exception e) {
            throw new HangmanException(e.getMessage());
        }
    }

    @Override
    public Game get(Long id) {
        return gameRepository.findOne((Specification<Game>) (root, cq, cb) -> cb.equal(root.get("id"), id)).orElse(null);
    }

    @Override
    public Game create(Game game) {
        game.setGuesses(0);
        game.setGuessesLeft(9);
        game.setIncorrectLetters("");
        game.setGameStatus(GameStatus.RESUMING.getKey());
        return gameRepository.save(game).orElse(new Game());
    }

    @Override
    public Boolean delete(Long id) throws HangmanException {
        try {
            Game game = new Game();
            game.setId(id);
            gameRepository.delete(game);
            return true;
        } catch (Exception e) {
            throw new HangmanException(e.getMessage());
        }

    }

    @Override
    public Game update(Game game) throws HangmanException {
        Game tempGame = this.get(game.getId());
        if (tempGame == null) {
            throw new HangmanException("Update edilecek game bulunamadı.");
        }
        return gameRepository.save(game).orElse(new Game());
    }
}
