package com.cancanbay.hangman.service;

import com.cancanbay.hangman.exception.HangmanException;
import com.cancanbay.hangman.model.Player;
import com.cancanbay.hangman.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlayerService extends AbstractService implements IPlayerService {
    @Autowired
    private PlayerRepository playerRepository;

    @Override
    public List<Player> getAll() throws HangmanException {
        try {
            return playerRepository.findAll();
        } catch (Exception e) {
            throw new HangmanException(e.getMessage());
        }
    }

    @Override
    public Player get(Long id) {
        return playerRepository.findOne((Specification<Player>) (root, cq, cb) -> cb.equal(root.get("id"), id)).orElse(null);
    }

    @Override
    public Player create(Player player) {
        return playerRepository.save(player).orElse(new Player());
    }

    @Override
    public Boolean delete(Long id) throws HangmanException {
        try {
            Player player = new Player();
            player.setId(id);
            playerRepository.delete(player);
            return true;
        } catch (Exception e) {
            throw new HangmanException(e.getMessage());
        }

    }

    @Override
    public Player update(Player player) throws HangmanException {
        Player tempPlayer = this.get(player.getId());
        if (tempPlayer == null) {
            throw new HangmanException("Update edilecek player bulunamadı.");
        }
        return playerRepository.save(player).orElse(new Player());
    }
}
