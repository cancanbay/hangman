package com.cancanbay.hangman.service;

import com.cancanbay.hangman.model.Game;

public interface IGameService extends IGenericService<Game, Long> {

}
