package com.cancanbay.hangman.service;

import com.cancanbay.hangman.exception.HangmanException;

import java.util.List;

public interface IGenericService<T, ID> {
    List<T> getAll() throws HangmanException;

    T get(ID id) throws HangmanException;

    T create(T entity) throws HangmanException;

    Boolean delete(ID id) throws HangmanException;

    T update(T entity) throws HangmanException;
}
