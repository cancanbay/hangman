package com.cancanbay.hangman.repository;

import com.cancanbay.hangman.model.Player;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository extends GenericRepository<Player, Long> {
}
