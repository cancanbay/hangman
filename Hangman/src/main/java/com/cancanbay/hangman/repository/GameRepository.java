package com.cancanbay.hangman.repository;

import com.cancanbay.hangman.model.Game;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepository extends GenericRepository<Game, Long> {
}
