package com.cancanbay.hangman.repository;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface GenericRepository<T, ID> extends Repository<T,ID> {
    List<T> findAll();

    Optional<T> findOne(Specification<T> specification);

    Optional<T> save(T entity);

    void delete(T entity);
}
