package com.cancanbay.hangman.controller;

import com.cancanbay.hangman.exception.HangmanException;
import com.cancanbay.hangman.model.Error;
import com.cancanbay.hangman.response.GenericResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;

public abstract class AbstractApiController {

    public ResponseEntity<GenericResponse> getGenericResponseResponseEntity(HttpStatus status, String errorDescription, @Nullable Object genericObject) throws HangmanException {
        GenericResponse response = new GenericResponse();
        Error error = new Error();
        error.setErrorCode(status.value());
        error.setErrorMessage(errorDescription);
        GenericResponse mappedResponse =  response.getGenericResponse(error,status,genericObject);
        return new ResponseEntity<>(mappedResponse,status);
    }
}
