package com.cancanbay.hangman.controller;

import com.cancanbay.hangman.exception.HangmanException;
import com.cancanbay.hangman.model.Game;
import com.cancanbay.hangman.response.GenericResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

public interface IGameApiController extends IGenericApiController<Game, Long> {
    ResponseEntity<GenericResponse> put(@RequestBody Game requestGame, @PathVariable("gameId") Long id) throws HangmanException;
}
