package com.cancanbay.hangman.controller;

import com.cancanbay.hangman.enumeration.GameStatus;
import com.cancanbay.hangman.exception.HangmanException;
import com.cancanbay.hangman.model.Game;
import com.cancanbay.hangman.response.GenericResponse;
import com.cancanbay.hangman.service.GameService;
import com.cancanbay.hangman.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("hangman/v1/api/game")
public class GameApiController extends AbstractApiController implements IGameApiController {
    @Autowired
    private GameService gameService;

    @GetMapping("")
    @Override
    public ResponseEntity<GenericResponse> getAll() throws HangmanException {
        try {
            if (gameService.getAll().size() > 0) {
                return getGenericResponseResponseEntity(HttpStatus.OK,Constants.GAMES_SUCCESSFULLY_RETRIEVED,gameService.getAll());
            }
            return getGenericResponseResponseEntity(HttpStatus.NOT_FOUND,Constants.GAME_NOT_FOUND,gameService.getAll());
        } catch (Exception e) {
            return getGenericResponseResponseEntity(HttpStatus.BAD_REQUEST,Constants.UNEXPECTED_ERROR,null);
        }
    }


    @GetMapping("/{gameId}")
    @Override
    public ResponseEntity<GenericResponse> get(@PathVariable("gameId") Long id) throws HangmanException {
        try {
            Game game = gameService.get(id);
            if (game != null) {
                game.setWord(null);
                return getGenericResponseResponseEntity(HttpStatus.OK,Constants.GAME_INFO_SUCCESSFULLY_RETRIEVED,gameService.get(id));
            }
                return getGenericResponseResponseEntity(HttpStatus.NOT_FOUND,Constants.GAME_NOT_FOUND,null);
        } catch (Exception e) {
            return getGenericResponseResponseEntity(HttpStatus.BAD_REQUEST,Constants.UNEXPECTED_ERROR,null);
        }
    }


    @PostMapping("")
    @Override
    public ResponseEntity<GenericResponse> create(@RequestBody Game game) throws HangmanException {
        try {
            return getGenericResponseResponseEntity(HttpStatus.OK,Constants.GAME_SUCCESSFULLY_CREATED,gameService.create(game).getId()>0);
        } catch (Exception e) {
            return getGenericResponseResponseEntity(HttpStatus.BAD_REQUEST,Constants.UNEXPECTED_ERROR,Boolean.FALSE);
        }
    }

    @DeleteMapping("/{gameId}")
    @Override
    public ResponseEntity<GenericResponse> delete(@PathVariable("gameId") Long id) throws HangmanException {
        try {
            Game game = gameService.get(id);
            if (game != null) {
                return getGenericResponseResponseEntity(HttpStatus.OK,Constants.GAME_ENDED,gameService.delete(id));
            }
            return getGenericResponseResponseEntity(HttpStatus.NOT_FOUND,Constants.GAME_NOT_FOUND,Boolean.FALSE);
        } catch (Exception e) {
            return getGenericResponseResponseEntity(HttpStatus.BAD_REQUEST,Constants.UNEXPECTED_ERROR,Boolean.FALSE);
        }
    }

    @PutMapping("/{gameId}")
    @Override
    public ResponseEntity<GenericResponse> put(@RequestBody Game requestGame, @PathVariable("gameId") Long id) throws HangmanException {
        try {
            Game game = gameService.get(id);
            game.setGameStatus(GameStatus.RESUMING.getKey());
            String word = game.getWord();
            if (game == null) {
                return getGenericResponseResponseEntity(HttpStatus.NOT_FOUND,Constants.GAME_NOT_FOUND,null);
            }
            game.setGuesses(game.getGuesses() + 1);
            if (!isWordContainsLetter(requestGame.getGuessedWord(), word)) {
                game.setIncorrectLetters(game.getIncorrectLetters() + requestGame.getGuessedWord());
                game.setGuessesLeft(game.getGuessesLeft() - 1);
                lostGame(game, word);
                gameService.update(game);
                Game tempGame = mapToUpdatedGame(game);
                return getGenericResponseResponseEntity(HttpStatus.OK,Constants.GUESS_SUCCESSFULLY_DONE,tempGame);
            }
            StringBuilder result = new StringBuilder();
            String guessedWord = game.getGuessedWord();
            for (int i = 0; i < word.length(); i++) {
                result.append(requestGame.getGuessedWord().equals("" + word.charAt(i)) ? requestGame.getGuessedWord() : guessedWord.charAt(i));
            }
            game.setGuessedWord(result.toString());
            winGame(game);
            gameService.update(game);
            Game tempGame = mapToUpdatedGame(game);
            return getGenericResponseResponseEntity(HttpStatus.OK,Constants.GUESS_SUCCESSFULLY_DONE,tempGame);
        } catch (Exception e) {
            return getGenericResponseResponseEntity(HttpStatus.BAD_REQUEST,Constants.GUESS_FAILED,null);
        }
    }

    private void winGame(Game game) {
        if(!game.getGuessedWord().contains("*") && game.getGuessesLeft().compareTo(0) > 0){
            game.setGameStatus(GameStatus.WON.getKey());
        }
    }

    private void lostGame(Game game, String word) {
        if(game.getGuessesLeft().compareTo(0) == 0){
            game.setGameStatus(GameStatus.LOST.getKey());
            game.setWord(word);
            game.setGuessedWord(word);
        }
    }

    private Game mapToUpdatedGame(Game game) {
        Game tempGame = new Game();
        tempGame.setWord(game.getWord());
        tempGame.setGuessedWord(game.getGuessedWord());
        tempGame.setIncorrectLetters(game.getIncorrectLetters());
        tempGame.setGuessesLeft(game.getGuessesLeft());
        tempGame.setGuesses(game.getGuesses());
        tempGame.setGameStatus(game.getGameStatus());
        return tempGame;
    }

    private boolean isWordContainsLetter(String letter, String word) {
        return word.contains(letter);
    }

}
