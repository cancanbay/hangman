package com.cancanbay.hangman.controller;

import com.cancanbay.hangman.exception.HangmanException;
import com.cancanbay.hangman.response.GenericResponse;
import org.springframework.http.ResponseEntity;

public interface IGenericApiController<T,ID> {
    ResponseEntity<GenericResponse> getAll() throws HangmanException;

    ResponseEntity<GenericResponse> get(ID id) throws HangmanException;

    ResponseEntity<GenericResponse> create(T entity) throws HangmanException;

    ResponseEntity<GenericResponse> delete(ID id) throws HangmanException;
}
