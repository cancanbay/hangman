package com.cancanbay.hangman.controller;

import com.cancanbay.hangman.exception.HangmanException;
import com.cancanbay.hangman.model.Player;
import com.cancanbay.hangman.response.GenericResponse;
import com.cancanbay.hangman.service.PlayerService;
import com.cancanbay.hangman.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("hangman/v1/api/player")
public class PlayerApiController extends AbstractApiController implements IPlayerApiController {
    @Autowired
    private PlayerService playerService;


    @GetMapping("")
    @Override
    public ResponseEntity<GenericResponse> getAll() throws HangmanException {
        try {
            if(playerService.getAll().size() > 0) {
                return getGenericResponseResponseEntity(HttpStatus.OK,Constants.PLAYER_SUCCESSFULLY_RETRIEVED,playerService.getAll());
            }
            return getGenericResponseResponseEntity(HttpStatus.NOT_FOUND,Constants.PLAYER_NOT_FOUND,playerService.getAll());
        } catch (Exception e) {
            return getGenericResponseResponseEntity(HttpStatus.BAD_REQUEST,Constants.UNEXPECTED_ERROR,null);
        }
    }

    @GetMapping("/{playerId}")
    @Override
    public ResponseEntity<GenericResponse> get(@PathVariable("playerId") Long id) throws HangmanException {
        try {
            Player player = playerService.get(id);
            if(player != null) {
                return getGenericResponseResponseEntity(HttpStatus.OK,Constants.PLAYER_INFO_SUCCESSFULLY_RETRIEVED,playerService.get(id));
            }
            return getGenericResponseResponseEntity(HttpStatus.NOT_FOUND,Constants.PLAYER_NOT_FOUND,null);
        } catch (Exception e) {
            return getGenericResponseResponseEntity(HttpStatus.BAD_REQUEST,Constants.UNEXPECTED_ERROR,null);
        }
    }

    @PostMapping("")
    @Override
    public ResponseEntity<GenericResponse> create(@RequestBody Player player) throws HangmanException {
        try {
            List<String> playerNames = new ArrayList<>();
            for(Player localPlayer : playerService.getAll()){
                playerNames.add(localPlayer.getName());
            }
            if(playerNames.contains(player.getName())){
                return getGenericResponseResponseEntity(HttpStatus.BAD_REQUEST,Constants.PLAYER_NAME_ALREADY_EXISTS,Boolean.FALSE);
            }
            return getGenericResponseResponseEntity(HttpStatus.OK,Constants.PLAYER_SUCCESSFULLY_CREATED,playerService.create(player).getId() > 0);
        } catch (Exception e) {
            return getGenericResponseResponseEntity(HttpStatus.BAD_REQUEST,Constants.UNEXPECTED_ERROR,Boolean.FALSE);
        }
    }

    @DeleteMapping("/{playerId}")
    @Override
    public ResponseEntity<GenericResponse> delete(@PathVariable("playerId") Long id) throws HangmanException {
        try {
            Player player = playerService.get(id);
            if(player != null) {
                return getGenericResponseResponseEntity(HttpStatus.OK,Constants.PLAYER_SUCCESSFULLY_DELETED,playerService.delete(id));
            }
            return getGenericResponseResponseEntity(HttpStatus.NOT_FOUND,Constants.PLAYER_NOT_FOUND,Boolean.FALSE);
        } catch (Exception e) {
            return getGenericResponseResponseEntity(HttpStatus.BAD_REQUEST,Constants.UNEXPECTED_ERROR,Boolean.FALSE);
        }
    }
}
