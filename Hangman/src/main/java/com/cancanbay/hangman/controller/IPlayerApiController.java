package com.cancanbay.hangman.controller;

import com.cancanbay.hangman.model.Player;

public interface IPlayerApiController extends IGenericApiController<Player, Long> {
}
