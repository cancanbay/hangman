package com.cancanbay.hangman.controller;

import com.cancanbay.hangman.model.Game;
import com.cancanbay.hangman.model.Player;
import com.cancanbay.hangman.service.GameService;
import com.cancanbay.hangman.service.PlayerService;
import com.cancanbay.hangman.util.ResourceUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

@Controller
@RequestMapping("hangman/v1")
public class HomeController {
    @Autowired
    private PlayerService playerService;
    @Autowired
    private ResourceUtils resourceUtils;

    @Autowired
    private GameService gameService;

    @GetMapping("")
    public String index(Model model) {
        model.addAttribute("player", new Player());
        return "index";
    }

    @GetMapping("/game/{gameId}")
    public String startGame(@PathVariable("gameId") Long gameId) {
        Game game = gameService.get(gameId);
        if(game == null){
            return "error";
        }
        return "game";
    }

    @PostMapping("/game")
    public String handleForm(@ModelAttribute Player player) {
        Player tempPlayer = playerService.create(player);
        Game game=new Game();
        game.setPlayer(tempPlayer.getName());
        Random random = new Random();
        List<String> words = resourceUtils.getListFromResource();
        String randomWord = words.get(random.nextInt(words.size()));
        game.setWord(randomWord);
        StringBuilder sbGuessedWord = new StringBuilder();
        for(int i=0;i<randomWord.length();i++){
            sbGuessedWord.append("*");
        }
        game.setGuessedWord(sbGuessedWord.toString());
        Game tempGame =  gameService.create(game);
        if (tempPlayer.getId() > 0 && tempGame.getId() > 0) {
            return "redirect:/hangman/v1/game/"+tempGame.getId();
        }
        return "error";
    }
}
