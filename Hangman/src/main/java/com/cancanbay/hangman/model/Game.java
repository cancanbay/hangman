package com.cancanbay.hangman.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Entity
@Table(name = "games")
public class Game extends AbstractModel<Long> {
    private String player;
    private Integer guesses;
    private Integer guessesLeft;
    private String guessedWord;
    private String incorrectLetters;
    private String word;
    private String gameStatus;

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public Integer getGuesses() {
        return guesses;
    }

    public void setGuesses(Integer guesses) {
        this.guesses = guesses;
    }

    public Integer getGuessesLeft() {
        return guessesLeft;
    }

    public void setGuessesLeft(Integer guessesLeft) {
        this.guessesLeft = guessesLeft;
    }

    public String getGuessedWord() {
        return guessedWord;
    }

    public void setGuessedWord(String guessedWord) {
        this.guessedWord = guessedWord;
    }

    public String getIncorrectLetters() {
        return incorrectLetters;
    }

    public void setIncorrectLetters(String incorrectLetters) {
        this.incorrectLetters = incorrectLetters;
    }

    public String getWord() {
        return this.word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(String gameStatus) {
        this.gameStatus = gameStatus;
    }
}
