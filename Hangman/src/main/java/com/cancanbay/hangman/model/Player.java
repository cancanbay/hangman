package com.cancanbay.hangman.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Entity
@Table(name = "players")
public class Player extends AbstractModel<Long> {
    @Column(unique = true, nullable = false)
    private String name;
    @Column(nullable = false)
    private Integer age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
