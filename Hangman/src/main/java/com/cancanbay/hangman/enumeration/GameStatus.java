package com.cancanbay.hangman.enumeration;

public enum GameStatus {

    LOST("Kaybedildi", -1), RESUMING("Devam Ediyor", 0), WON("Kazanıldı", 1),;

    private final String key;
    private final Integer value;

    GameStatus(String key, Integer value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public Integer getValue() {
        return value;
    }
}