package com.cancanbay.hangman.response;

import com.cancanbay.hangman.exception.HangmanException;
import com.cancanbay.hangman.model.Error;
import org.springframework.http.HttpStatus;

public class GenericResponse<T> {
    private Error error;
    private T genericObject;
    private HttpStatus status;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public T getGenericObject() {
        return genericObject;
    }

    public void setGenericObject(T genericObject) {
        this.genericObject = genericObject;
    }


    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public GenericResponse getGenericResponse(Error error, HttpStatus status, T genericObject) throws HangmanException {
        GenericResponse response = new GenericResponse();
        response.setError(error);
        response.setStatus(status);
        response.setGenericObject(genericObject);
        return response;
    }
}

