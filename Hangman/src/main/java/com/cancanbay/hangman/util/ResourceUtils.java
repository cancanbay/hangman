package com.cancanbay.hangman.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Component
public class ResourceUtils {
    public List<String> getListFromResource() {
        try {
            Resource resource = new ClassPathResource("static/json/sozluk.json");
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(resource.getFile(), List.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
}
