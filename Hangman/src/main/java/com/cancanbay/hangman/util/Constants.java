package com.cancanbay.hangman.util;

public class Constants {
    public static final String GAMES_SUCCESSFULLY_RETRIEVED = "Oyunlar başarıyla çekildi.";
    public static final String UNEXPECTED_ERROR = "Beklenmeyen hata oluştu";
    public static final String GAME_SUCCESSFULLY_CREATED = "Oyun başarıyla başlatıldı.";
    public static final String GAME_INFO_SUCCESSFULLY_RETRIEVED = "Oyun bilgisi başarıyla getirildi.";
    public static final String GUESS_SUCCESSFULLY_DONE = "Tahmin başarıyla yapıldı.";
    public static final String GUESS_FAILED = "Tahmin yapılamadı.";
    public static final String GAME_ENDED = "Oyun sonlandırıldı.";
    public static final String GAME_NOT_FOUND = "Oyun bulunamadı.";
    public static final String PLAYER_SUCCESSFULLY_RETRIEVED = "Oyuncu başarıyla çekildi.";
    public static final String PLAYER_NAME_ALREADY_EXISTS = "Oyuncu adı zaten var.";
    public static final String PLAYER_NOT_FOUND = "Oyuncu bulunamadı.";
    public static final String PLAYER_SUCCESSFULLY_CREATED = "Oyuncu başarıyla yaratıldı.";
    public static final String PLAYER_INFO_SUCCESSFULLY_RETRIEVED = "Oyuncu bilgisi çekildi";
    public static final String PLAYER_SUCCESSFULLY_DELETED = "Oyuncu başarıyla silindi";
}
